﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "The Animal made sound!\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!\n";
    }
};

class Duck : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Quack!\n";
    }
};

class Goat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Bleat!\n";
    }
};

class Bee : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Buzz!\n";
    }
};

class Fox : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Horrible scream!\n";
    }
};


int main()
{
    
    Animal* A[] = { new Dog, new Cat, new Cow, new Duck, new Goat, new Bee,  new Fox };

        for (auto& element : A)
        {
            element->Voice();
        }
}
